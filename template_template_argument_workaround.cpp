template <template <typename...> class C, typename Head, typename... Tail>
struct instantiate_with_tail {
    using type = C<Tail...>;
};

template<typename... Ts>
class Foo {
public:
    using next_type = typename instantiate_with_tail<::Foo, Ts...>::type;
};

template<>
class Foo<> {
};

int main() {
    Foo<int> a;
}
